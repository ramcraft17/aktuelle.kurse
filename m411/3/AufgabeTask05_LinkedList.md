### Aufgabe/Task: Nr. 05

Thema: Linked List / Verkettete Liste

Geschätzter Zeitbedarf: 30-70min pro Aufgabe

#### Aufgabenbeschreibung:

Zur Unterstützung und weitere Anleitung, schauen Sie sich dieses Video an.

<https://www.youtube.com/watch?v=i2v_Ve9PUCw>

Bauen Sie gemäss dieser Anleitung eine (einfache) Verlinkte Liste.

- [script3_2_dynamicStructures_linkedList.pdf](./script3_2_dynamicStructures_linkedList.pdf)
- [script3_2_dynamischeStrukturen_verkListe.pdf](./script3_2_dynamischeStrukturen_verkListe.pdf)



Bewertung: Keine, ist aber prüfungsrelevant
