### Aufgabe/Task: Nr. 02

Thema: Datenstrukturen

Geschätzter Zeitbedarf: 20-30min

#### Aufgabenbeschreibung:

1.) Schauen Sie sich dieses Video an und schreiben Sie mit, welche
Datenstrukturen vorgestellt werden.

*Alles werden Sie im Laufe des Moduls selber einmal ausprobieren. Aber das dann
später. Hier erst mal schauen und analysieren, worum es geht.*

 - <https://www.youtube.com/watch?v=sVxBVvlnJsM>

2.) Wozu dienen Datenstrukturen (wozu braucht man das)?

*Sie dürfen auch mit Klassenkameraden darüber diskutieren. Aber jede:r schreibt
seinen eigenen Text*




Bewertung: Es gibt Punkte, die sind aber nicht Modulnotenrelevant. Sie machen aber eine
Aussage, wie gut die Lehrperson Ihr Text findet.
