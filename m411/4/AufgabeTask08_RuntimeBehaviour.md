### Aufgabe/Task: Nr. 08

Thema: Runtime Behaviour

Geschätzter Zeitbedarf: 120-240 min

#### Aufgabenbeschreibung:

**Teil 1**

Bearbeiten Sie diese Übungsanleitung(en):

- [script4_firstStepsPerformance.pdf](./script4_firstStepsPerformance.pdf)
- [script4_ErsteSchrittePerformance.pdf](./script4_ErsteSchrittePerformance.pdf)


Verwenden Sie diesen Code (stopwatch) um die Schnelligkeit(en) zu messen.  
```java
/**
 * Used from the book by R. Sedgwick.
 * @author littleJ
 *
 */

public class Stopwatch {
	
	private final long start;

	   /**
	     * Create a stopwatch object.
	     */
	    public Stopwatch() {
	        start = System.currentTimeMillis();
	    } 


	   /**
	     * Return elapsed time (in seconds) since this object was created.
	     */
	    public double elapsedTime() {
	        long now = System.currentTimeMillis();
	        return (now - start) / 1000.0;
	    }
}
```

**Teil 2**

Finden Sie in den ‘normalen’ Java-Board-Mitteln Datenstrukturen, mit denen Sie
sammeln und sortieren lassen können (z.B. collections, arrays, u.a.) und machen
Sie (statistische) Vergleiche mit dem früher implementierten BubbleSort.
Verwenden Sie dabei Ihren Zahlengenerator aus der früheren Übung und nehmen Sie
genügend viele Zahlen, messen Sie die Schnelligkeiten und werten Sie sie mit
Excel aus.

<https://www.geeksforgeeks.org/arrays-sort-in-java-with-examples/>

**Teil 3**

Finden Sie weitere Sort-Algorithmen im Internet und implementieren Sie einen,
den Sie noch nicht kennen davon und vergleichen Sie ebenfalls die Leistung
(Performanz) dieses Algorithmus. (z.B. QuickSort o. a.).

**Zeigen Sie die Resultate der Lehrperson**
