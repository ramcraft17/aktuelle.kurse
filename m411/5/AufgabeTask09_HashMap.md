### Aufgabe/Task: Nr. 09

Thema: HashMaps in Java

Geschätzter Zeitbedarf: 60-120 min

#### Aufgabenbeschreibung:

Schauen Sie sich zwei der angebotenen Videos über Java HashMap an

- [Videos-Tutorials-Anleitungen](../docs/Videos-Tutorials-Anleitungen)
  - <https://bscw.tbz.ch/bscw/bscw.cgi/32213665>  
- [HashMapUndRekursion.pdf](./HashMapUndRekursion.pdf)
- [HashMapUndRekursion.pptx](./HashMapUndRekursion.pptx)


 - HashMap Java Tutorial <https://www.youtube.com/watch?v=70qy6_gw1Hc>


.. und bauen Sie in Java eine eigene HashMap nach und geben Sie den Java-Code im
Teams ab oder zeigen Sie es der Lehrperson.


Bewertung: Keine, ist aber prüfungsrelevant
