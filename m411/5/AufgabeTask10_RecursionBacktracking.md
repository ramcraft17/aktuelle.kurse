### Aufgabe/Task: Nr. 10

Thema: Recursion and Backtracking

Geschätzter Zeitbedarf: 120-150 min

#### Aufgabenbeschreibung:

Studieren Sie folgendes Dokument, indem Sie alles nachbauen, was drin vorkommt.
Versuchen Sie insbesondere die Iteration in eine Rekursion umzubauen.

- [script7_rekursionUndBacktracking.pdf](./script7_rekursionUndBacktracking.pdf)
- [script7c_vonIterationZuRekursion.pdf](./script7c_vonIterationZuRekursion.pdf)
- [HashMapUndRekursion.pdf](./HashMapUndRekursion.pdf)
- [HashMapUndRekursion.pptx](./HashMapUndRekursion.pptx)


Videos:
- [Videos-Tutorials-Anleitungen](../docs/Videos-Tutorials-Anleitungen)

Code-Stücke zum Anwenden:
- [Rekursion: DiskUsage.java](../docs/Daten-Uebungen-CodeBeispiele/Rekursion/DiskUsage.java) 
- [Backtracking: SudokoSolver.java](../docs/Daten-Uebungen-CodeBeispiele/Backtracking(Sudoku)/SudokoSolver.java)



Bewertung: Keine, ist aber prüfungsrelevant
