# M306 - Kleinprojekte im eigenen Berufsumfeld abwickeln
(Definition BiVo 2014: **IT Kleinprojekte abwickeln**)

[> **Modulidentifikation** ](https://www.modulbaukasten.ch/modul/a875c9da-716c-eb11-b0b1-000d3a830b2b)

## Themen
| ~Tag | Thema                                                                          | Modulnote                 |
| ---- | ----                                                                           | ----                      |
|   1  | [Projekt-Ziele                        ](./1_Projekt-Ziele)                     |                           |
|   2  | [Projekt-Planung                      ](./2_Projekt-Planung)                   | Ecolm-Test P-Zie 7% MNote  |
|   3  | [Projekt-Organisation                 ](./3_Projekt-Organisation)              | Ecolm-Test P-Pla 7% MNote  |
|   4  | [Projekt-Führung                      ](./4_Projekt-Fuehrung)                  | Ecolm-Test P-Org 7% MNote  |
|   5  | [Projekt-Kontrolle und Vorgangsmodelle](./5_Projekt-Kontrolle-Vorgangsmodelle) | Ecolm-Test P-Füh 7% MNote  |
|   6  | [Projekt-Risiken und Risikoanalyse    ](./6_Projekt-Risiken-Risikoanalyse)     | Ecolm-Test P-Kon 7% MNote  |
|   7  | [Qualitätsicherung                    ](./7_Qualitaetsicherung)                | Gruppen-Projekt 32% MNote |
|      |                                        |                                       |                           |
|      | [Abschlussprojekt                     ](./Abschlussprojekt)                    | Gruppen-Projekt 33% MNote |                                       |
|      |                                        |                                       |                           |
|  add on 1  | [Lastenheft-Pflichtenheft](./add-on1_Lastenheft-Pflichtenheft)           |                           |
|  add on 2  | [Warum Projekte scheitern](./add-on2_Warum-Projekte-scheitern)           |                           |


![IT-Projectmanagement.jpg](./it-projectmanagement.jpg)

![Projekt-Zeitbedarf.jpg](./M306_Cartoon_Projekt-Zeitbedarf.jpg)
