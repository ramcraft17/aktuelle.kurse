# M306/Abschlussprojekt

**_(33 % Modulnote)_**


### Aufgabenstellung als Prüfungssituation für 4 Lektionen

**Themen/Lernziele:** 
 - Projektmanagement, 
 - Zusammenarbeiten im (Projekt-)Team, 
 - Rückblick auf alle Themen des Moduls

**Teamgrösse**
- 4-5 Personen

**Aufgabenstellung**
- Variante A: Abschlussprojekt als Erarbeitung eines Übersichtsdokuments [(PDF)](./M306_9_AbschlussprojektA_und_Dokumentenbewertung.pdf) / [(DOCX)](./M306_9_AbschlussprojektA_und_Dokumentenbewertung.docx)
- Variante B: [Organisation eines Triathlons](./M306_9_AbschlussprojektB_OrganisationEinesTriathlons.pdf)
- [Bewertungsblatt](./M306_9_Abschlussprojekt_Bewertung.docx)
