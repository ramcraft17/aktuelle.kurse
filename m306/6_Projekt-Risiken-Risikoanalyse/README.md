# M306/6 Projekt-Risiken und Risikoanalyse

- [Projektkontrolle-risiken_Praeinstruktion](./M306_6a_Projektkontrolle-risiken_Praeinstruktion.txt)

## Theorie
- [Skript (Seiten 50-51)](../docs) 
- (6:20 min) [Risiken im Projekt managen 1](./M306_6f_(Teil 1, 6.20 min)Risiken im Projekt managen.mp4)
- (5:07 min) [Risiken im Projekt managen 2](./M306_6f_(Teil 2, 5.07 min)Risiken im Projekt managen.mp4)
- (5:47 min) [Risiken im Projekt managen 3](./M306_6f_(Teil 3, 5.47 min)Risiken im Projekt managen.mp4)

## Praxis
- [Projektkontrolle/-Ablauf Praxisfragen](./M306_6b_ProjektkontrolleUndAblauf_Praxisfragen.pdf)
- [Zu viele strategische IT-Projekte sind krank-und daher gefährdet](./M306_6c_Zu viele strategische IT-Projekte sind krank-und daher gefährdet.pdf)

- [Risikoanalysetabelle](./M306_6d_Risikoanalysetabelle.png)
- [Risikofelder](./M306_6e_Risikofelder.pdf)

## Aufgabe
- [Übung Logic](../uebungen/M306_6g_ProjektkontrolleUndAblauf_Uebung_Logic.pdf)