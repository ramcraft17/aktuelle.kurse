# M306/4 Projekt-Führung

## Praxis 
- Offline-Erfahung "Teambildung" inkl. Video-Aufnahmen <br/>
(--> Struktur-Legen in Gruppen zu 5-7 Personen) <br />
Ressourcen: [Struktur-Lege-Karten](../vorlagen_beispiele/M306_StrukturLegeKarten.docx)

- Tabu-Spiel in Gruppen zu 5-7 Personen "Sitzungstypen" <br/>
(--> Papierstreifen bereitlegen)


## Theorie
- [Projektplanung_Praeinstruktion](./M306_4_Projektfuehrung_Praeinstruktion.txt)


- [Skript (Seiten 23-32)](../docs) 
**"Projektführung"** (entlang dem Theoriedokument)
  - Die Rolle des Projektleiters
  - Teambildung _**( --> Offline-Erfahrung )**_
  - Menschenbild/Sitzungstypen _**( --> Tabu-Spiel )**_
  - Sitzungsgestaltung
  - Pareto-Prinzip (80/20-Regel)
  - Eisenhower-Prinzip (wichtig/dringlich)
  - Entscheidungsfolgen-Matrix

