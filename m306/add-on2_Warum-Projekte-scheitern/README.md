## M306/8 Warum Projekte scheitern

 - [05:20 min, Gunkl: Die Vasa, der Stolz der schwedischen Marine](https://www.youtube.com/watch?v=ObIvBgkEGW4)
- - -  
 - [Warum Projekte scheitern.pdf](./Warum Projekte scheitern.pdf)
 - [Warum IT-Projekte scheitern](https://dieprojektmanager.com/scheitern-von-it-projekten)
- - - 
 - [Die schlimmsten Software-Pleiten der Schweizer Verwaltungen, 2011](https://www.tagesanzeiger.ch/digital/computer/die-schlimmsten-softwarepleiten-der-schweizer-verwaltungen/story/17751801)
 - [Gescheiterte IT-Projekte, 2014](https://www.computerwoche.de/a/gescheiterte-it-projekte,2546218)
 - [Astra schmeisst Trivadis raus, 2015](https://www.computerworld.ch/business/politik/astra-schmeisst-trivadis-raus-1338774.html)
 - [Und wieder ein gescheitertes IT-Projekt, 2015](https://hub.hslu.ch/informatik/und-wieder-ein-gescheitertes-it-projekt)
- - -
Recherchieren Sie weitere Stellen oder Beispiele im Internet über 
"IT-Flops", "gescheiterte IT Projekte" usw.
und versuchen Sie herauszufinden, **_warum_** Projekte scheitern.

Erstellen Sie in der Gruppe zu 3 Personen eine **_Checkliste_** der Gründe oder der Erkenntnisse zur Präsentation in der Klasse. 

