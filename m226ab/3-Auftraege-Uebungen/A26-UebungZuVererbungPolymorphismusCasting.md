# Uebung zu Vererbung, Polymorphismus, Casting 

Zeitbedarf ca. 40-60 min.

Laden Sie den Code herunter und lösen Sie alle Probleme die darin "versteckt" sind indem Sie auch die Kommentare ganz genau lesen und die Empfehlungen dort verfolgen.
<https://gitlab.com/harald.mueller/java_m226>
Geben Sie hier im Auftrag alle vier  *.java*-Files ab (alle einzeln - kein zip-File !)
