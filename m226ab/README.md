# M226a - Klassenbasiert (ohne Vererbung) implementieren
# M226b - Objektorientiert (mit Vererbung) implementieren
[> **Modulidentifikation M226a** ](https://cf.ict-berufsbildung.ch/modules.php?name=Mbk&a=20101&cmodnr=226A&noheader=1)
<br>
[> **Modulidentifikation M226b** ](https://cf.ict-berufsbildung.ch/modules.php?name=Mbk&a=20101&cmodnr=226B&noheader=1)

## M226a LB1 (15%, mündliche Einzelprüfung, 12 min)
Themen: UML, OO-Prinzipien


## M226a LB2 (30%, schriftliche Prüfung, 60 min)
Themen: UML, OO-Prinzipien, ...


## M226a LB3 (55%, praktisches Projekt) 
Bewertungskriterien:<br>
Es müssen alle Elemente im Buch M226 von Ruggerio, Compendio von Kap. 5 bis 11, sowie Kap. 14 und 15 enthalten sein.
- Wer das Minimum des Kap. 13 macht, kann maximal die Note 4.7 erreichen.
- Wer ein eigenes Projekt "gut" abschliesst inkl. "Doku", "JavaDoc" und "JUnit-Tests", kann eine Note 6 machen. |


Bücher
- [./2-Unterlagen/00-Buecher/Buch__M226_Ruggerio_Compendio](./2-Unterlagen/00-Buecher/Buch__M226_Ruggerio_Compendio/)


UML-Tools
- https://staruml.io/download
- https://umletino.com



## M226a Plan, Aufträge, Übungen, Themen
| Tag  | Auftrag/Übung | Inhalte, Themen |
| ---- | ------------- | ------------------------ |
| 1    | [A11](./3-Auftraege-Uebungen/A11-Wissensaneignung1.md)            |  Modulvorstellung <br> Installation Eclipse oder ähnliche Programmierumgebung (Buch Kap. 12.1)<br> Beginn mit Buch/Skript Compendio 226 selbständig durchmachen (Teil A (Kap. 1-4))                     |
| 2    | [A12](./3-Auftraege-Uebungen/A12-WissensaneignungUML.md),[A13](./3-Auftraege-Uebungen/A12-GrundsaetzeDerOOProgrammierung.md)            |  Wissensaufbau mit Buch Compendio 226, selbständig durchmachen <br>*Input* JDK [Q](./2-Unterlagen/01-Einfuehrung-OOP-Klassen-Objekte/M226_Einfuehrung_JDK.pdf) & [A](./2-Unterlagen/01-Einfuehrung-OOP-Klassen-Objekte/M226_Einfuehrung_JDK_Anworten.pdf) <br>*Input* Klassen und Objekte [Q](./2-Unterlagen/01-Einfuehrung-OOP-Klassen-Objekte/M226_Klassen_Objekte.pdf) & [A](./2-Unterlagen/01-Einfuehrung-OOP-Klassen-Objekte/M226_Klassen_Objekte_loesungen.pdf)                                       |
| 3    |             |  Weiterarbeit am Wissensaufbau, <br>*Input* über UML-Zusammenhänge                                        |
| 4    |             |  **LB1** Definition eigenes Projektes (max 3 Pers)<br>(Projektumriss, Anforderungsdefinition, Activity-Diagram, Use-cases, ERM?, Class-Diagram, Sequence-Diagram)<br> Ab dem 2. Teil des Halbtages laufend Kompetenzabnahmen/Basic-Check (mündlich einzeln, Teil A im Buch)  |
| 5    |             |  Basic-Check (Fortsetzung) <br>Beginn mit dem eigenen Projekt (Planung/Konzept, UML). Lassen Sie sich von Kap. 13 inspirieren. Bedingung: Es müssen alle Elemente von Kap. 5 bis 11, sowie 14 und 15 enthalten sein.<br><br>Der LP die Aufgabenstellung aufzeigen. Diagramm(e) & Prosa   |
| 6    |             |  **LB2** Schriftliche Prüfung, 30% ModulNote<br>Weiterarbeit am Projekt                     |
| 7    |             |  Arbeit am Projekt<br>Präsentierung Zwischenstand des Projektes (v.a. eine Herausforderung)                     |
| 8    |             |  Arbeit am Projekt<br>Projektbeobachtung durch LP |
| 9    |             |  Arbeit am Projekt<br>Projektbeobachtung durch LP / erste Projektabnahmen  |
| 10   |             |  Arbeit am Projekt<br>Projektabschluss, Projektdemos<br>Projektbesprechung/Notengebung |



## M226b Plan, Aufträge, Übungen, Themen
| Tag  | Auftrag/Übung | Inhalte, Themen |
| ---- | ------------  | -------------- |
| 1    |             |                       |
| 2    |             |                       |
| 3    |             |                       |
| 4    |             |                       |
| 5    |             |                       |
| 6    |             |                       |
| 7    |             |                       |
| 8    |             |                       |
| 9    |             |                       |
| 10   |             |                       |
