# Aufgabe M152-i: Testen / Automatisierung & autom. Formulare ausfüllen
Zeitbedarf:

## Automatisierung
Für das Automatische Testen ist "Selenium" als (Firefox-)Plugin entwickelt worden. Das PlugIn geht jetzt auch für andere Browser. Damit kann man nicht nur testen, sondern automatisch Formulare befüllen, bzw. automatsch selbständig Internet-Webseiten bedienen lassen. Auch für Massen- und Speedtests anwendbar.
- https://www.selenium.dev
- https://de.wikipedia.org/wiki/Selenium

10 JavaScript Projects in 1 Hour - Coding Challenge
- https://www.youtube.com/watch?v=8GPPJpiLqHk&t=2250s


## Testen
Siehe Folien über Testen

- https://docs.google.com/presentation/d/162nRB0JJJ0Z-nZZ9JI7KretpL8K4rwBsKabXmiIvrzY/edit#slide=id.gc7f2a33041_0_52

Responsive Test (Übersicht)

- https://www.ionos.de/digitalguide/websites/webdesign/tools-fuer-den-responsive-test-ihrer-website

Gute Ressourcen über GUI-Testen (Ranorex)

- https://www.ranorex.com/resources
