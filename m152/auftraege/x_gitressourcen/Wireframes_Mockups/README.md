## Wireframes, Mockups

- [https://app.moqups.com](https://app.moqups.com)
- [https://thedigitalprojectmanager.com/de/die-10-besten-wireframe-tools](https://thedigitalprojectmanager.com/de/die-10-besten-wireframe-tools)
- [https://www.iqual.ch/de/internet-glossar/mockups](https://www.iqual.ch/de/internet-glossar/mockups)
- [https://www.webschmoeker.de/webdesign/unterschied-wireframe-und-mockup](https://www.webschmoeker.de/webdesign/unterschied-wireframe-und-mockup)
- [https://www.lucidchart.com/pages/de/was-sind-website-wireframes](https://www.lucidchart.com/pages/de/was-sind-website-wireframes)
- [https://www.iqual.ch/de/internet-glossar/wireframes](https://www.iqual.ch/de/internet-glossar/wireframes)
